<?php

namespace App\Services;

use League\Csv\Reader;

class CsvService
{
    /**
     * Retrieve an array of the people listed within the CSV database.
     *
     * @return array
     * @throws \Exception
     */
    public static function getPeople(): array
    {
        $file = getenv('CSV_FILE_PATH');

        if (file_exists($file) === false) {
            throw new \Exception('CSV file "' . $file . '" cannot be found.');
        }

        if (!ini_get('auto_detect_line_endings')) {
            ini_set('auto_detect_line_endings', '1');
        }

        $csv = Reader::createFromPath('people.csv', 'r');

        return explode("\n", trim($csv->getContent()));
    }
}
