<?php

namespace App\Services;

use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;

class SlackService
{
    /**
     * @param string $message
     * @return void
     * @throws \Exception
     */
    public static function message(string $message): void
    {
        $url = getenv('SLACK_URL');

        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception('Slack URL "' . $url . '" is invalid. Check the SLACK_URL .env entry.');
        }

        $client = new Client();

        $client->request('POST', $url, [
             'headers' => [
                 'content-type' => 'application/json',
                 'Accept' => 'application/json'
             ],
             RequestOptions::JSON => [
                 'text' => $message
             ]
        ]);
    }
}
