<?php

namespace App\Command;

use App\Services\SlackService;
use App\Services\CsvService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RandomStandupOrder extends Command
{
    /**
     * @return void
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('app:standup')
            ->setDescription('Display a random order of people for a standup.');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $people = CsvService::getPeople();

        shuffle($people);

        $output->writeln([
            'Order of standup',
            '================',
        ]);

        foreach ($people as $key => $person) {
            $i = $key + 1;
            $people[$key] = $i . '. ' . $person;

            $output->writeln($i . '. ' . $person);
        }

        SlackService::message(
            "*Standup*:\n" .
            implode("\n", $people)
        );
    }
}
