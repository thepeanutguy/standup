# Standup Bot

This is a simple bot that will send a randomised list of peopled to slack
for your daily stand-up 🎉.

## Requirements

- PHP 7.2

## Install

```bash
$ composer install
```

1. Copy the `.example.env` and name it `.env`.
2. Add your desired slack channel URL to the .env `SLACK_URL` variable.
3. Copy the `.people.example.csv` and name to whatever you like.
4. Add the filename to to the .env `CSV_FILE_PATH` variable.

## Run

```bash
$ php bot
```
